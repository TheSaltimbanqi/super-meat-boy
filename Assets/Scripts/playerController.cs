﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 11f;
    public float jump = 10f;
    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private bool right = false;
    private bool jumping = false;
    private Animator anim;

    private bool flipped = false;

    private bool alive = true;

    private string level1 = "level2";
    private string level2 = "level3";
    private string level3 = "level4";
    private string level4 = "finalScene";

    private float xPos = 0;
    private float yPos = 0;

    Vector3 restartPos;

    private float timer = 0;
    public float timeToWait = 1.5f;
    private bool checkTime = true;
    private bool timerDone = false;

    private AudioSource Audio;
    public AudioClip JumpSFX;
    public AudioClip DeathSFX;
    private bool musicPlaying = false;

    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();

        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();

        Audio = GetComponent<AudioSource>();
    }

    private void Start()
    {
        restartPos = transform.position;
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(5);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (alive)
        {
            //Miramos el input Horizontal
            move = Input.GetAxis("Horizontal");

            rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

            if (Input.GetKeyDown("space") && !jumping)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, jump);
                Audio.clip = JumpSFX;
                Audio.Play();

                jumping = true;
                anim.SetBool("jump", true);
            }

            //Miramos si nos estamos moviendo.
            // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
            if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
            {
                if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
                {
                    flipped = !flipped;
                    this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
                }
                anim.SetBool("walk", true);
            }
            else
            {
                anim.SetBool("walk", false);
            }
        }
        else if (!alive)
        {
            anim.SetBool("death", true);
            if (!musicPlaying)
            {
                Audio.clip = DeathSFX;
                Audio.Play();
                musicPlaying = true;
            }

            if (checkTime)
            {
                timer += Time.deltaTime;
                if (timer >= timeToWait)
                {
                    timerDone = true;
                    timer = 0;
                }
            }
            if (timerDone) {
                Audio.Stop();
                musicPlaying = false;
                transform.position = restartPos;
                anim.SetBool("death", false);
                alive = true;
                timerDone = false;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "trampa")
        {
            alive = false;
        }
        
        jumping = false;
        anim.SetBool("jump", false);
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "lv1")
        {
            SceneManager.LoadScene(level1);
        }
        else if (coll.gameObject.tag == "lv2")
        {
            SceneManager.LoadScene(level2);
        }
        else if (coll.gameObject.tag == "lv3")
        {
            SceneManager.LoadScene(level3);
        }
        else if (coll.gameObject.tag == "lv4")
        {
            SceneManager.LoadScene(level4);
        }
    }
}
